#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>

/* -O3 */

#ifndef N
#define N 512
#endif

#ifndef T
#define T 32
#endif

#ifndef NUM_REPS
#define NUM_REPS 1
#endif

#define min(a,b) ( ( (a) < (b) ) ? (a) : (b))

static double a[N][N], b[N][N], c[N][N];

void myGetTime(double *t, double *tlib)
{ struct rusage tmpru;

  getrusage(RUSAGE_SELF , &tmpru);
  *t = tmpru.ru_utime.tv_sec+(tmpru.ru_utime.tv_usec/1000000.0);
  *tlib = tmpru.ru_stime.tv_sec+(tmpru.ru_stime.tv_usec/1000000.0);
}

double test_reset() 
{ double t = 0.0;
  int i, j;

  for(i = 0; i < N ; i++)
    for(j = 0; j < N; j++) {
      t += c[i][j];
      c[i][j] = 0.0;
    }

  return t;
}

int main()
{ double f1, fl1, f2, fl2;
  int x,y,z, i, j, r, yy, zz, acum;
  
#ifndef NOINIT
  for(i = 0; i < N ; i++)
    for(j = 0; j < N; j++) {
      c[i][j] = 0.0;
      a[i][j] = i + j;
      b[i][j] = i * j;
    }
#endif

  myGetTime(&f1, &fl1);

  for(r = 0; r < NUM_REPS; r++) {

		for( yy=0; yy<N; yy+=T)
			for( zz=0; zz<N; zz+=T)
				for( x=0; x<N; x++)
					for( y=yy; y<min(yy+T,N); y++) {
						acum = c[x][y];
						for( z=zz; z<min(zz+T,N); z++)
							acum += a[x][z] * b[z][y];
						c[x][y] = acum;
					}
   
  }
  
  myGetTime(&f2, &fl2);

  printf("Tu: %lf  Tlib: %lf\n", f2-f1, fl2-fl1);
  printf("Test: %lf\n", test_reset());

}
