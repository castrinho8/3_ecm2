.text
.globl main

main:

la $a0 , cadena
jal longitud

addi $a0 , $v0, 0
addi $v0 , $0 , 1
syscall

addi $v0 , $0 , 10
syscall

longitud:
addi $t0, $a0, 0
addi $t1, $0 , 0
lb $t3, 0($t0)

loop:
beq $0, $t3, salir
addi $t0, $t0, 1
addi $t1, $t1, 1
lb $t3, 0($t0)
j loop

salir:
add $v0, $t1, $0
jr $ra

.data
cadena: .asciiz "simula3MS"
