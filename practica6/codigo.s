#programa 

.text
.globl main


#--------------------
# Programa principal
#-------------------

main:
  la $a0, cadena	#Cargamos la direccion.
  
  jal readstring
  
  add $a1, $0, $v0
  
  jal printstring
  
  addi $v0 , $0 , 10
  syscall
  

#----------------------
#SUBRUTINA ReadString
#----------------------

readstring:

  add $t1, $0, $a0	# Metemos en $t1 la direccion de memoria donde se guarda la cadena.
  add $t2, $0, $0	# Ponemos el contador a 0
  
  addi $t5, $0, 0x3b	#Metemos en $t5 el caracter ESC (0x3b)
  
  li $t0, 0xffff0000

ckready:
  lw $t6, 0($t0)
  andi $t6, $t6, 1
  beq $t6, $0, ckready

  li $t3, 0xffff0004   #Se carga la direccion donde se almacena el caracer escrito por teclado.
  lb $t4, 0($t3)
  
  beq $t4, $t5, terminarrs   #Si el caracter es ESC (0x3b) se sale del programa.
  
  sb $t4, 0($t1) 	#Guardamos el caracter en la cadena.
  addi $t1, $t1, 1 	#Se aumenta el "indice" de la cadena.
  addi $t2, $t2, 1 
 
  j ckready
  
  
terminarrs:
  add $t5, $0, $0  #Metemos en el reg $t5 el caracter \0 (fin de cadena).
  sb $t4, 0($t1)   #Lo almacenamos en la cadena.
  
  add $v0, $0, $t2 
  
  jr $ra
  
  
#----------------------
#SUBRUTINA PrintString
#----------------------

printstring:

  add $t1, $0, $a1 #Almacenamos el tamaño en $t0
  add $t2, $0, $a0 #Almacenamos la direccion de la cadena.
  
  li $t0, 0xffff0000

xready:
  beq $t1, $0, salirps
  
  lw $t3, 8($t0)
  andi $t3, $t3, 1
  beq $t3, $0, xready

  li $t4, 0xffff000C #Cargamos la direccion para imprimir
  lb $t5, 0($t2)     #Cargamos el caracter correspondiente
  sb $t5, 0($t4)     # Lo almacenamos en la direccion.
  
  addi $t2, $t2, 1
  addi  $t1, $t1, -1 
  
  j xready
  
salirps:
  jr $ra
  
  

.data 
  cadena: .asciiz "                                                                                                 "