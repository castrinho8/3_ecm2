/* programa traspuesta.c */

#define N 200

double a[N][N], b[N][N]; 

int main() { 
	int i, j;
	for(i = 0; i < N; i++) //Lectura e escritura -> 200 L e E
		for(j = 0; j < N; j++) { //Lectura e escritura -> 40000 L e E
			a[i][j] = (double)(i * j); // Escritura e Lectura 40000 E + 80000 (i e j) L 
			b[j][i] = a[i][j]; // Lectura e Escritura 40000 L e E 40000 + 80000 L 
		}
}

/*

E -> 200 + 40000 + 40000 + 40000
L -> 200 + 40000 + 80000 + 40000 + 80000



*/
