#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>

/* -O3 */

#ifndef N
#define N 30000
#endif

#ifndef INDIRS
#define INDIRS 900000
#endif

#ifndef NUM_REPS
#define NUM_REPS 1
#endif


struct miregistro1 {
  short int a, b1, b2, b3, b4, b5, c;
  char relleno[2];
};


static struct miregistro1 v[N];
static int indices[INDIRS];

void myGetTime(double *t, double *tlib)
{ struct rusage tmpru;

  getrusage(RUSAGE_SELF , &tmpru);
  *t = tmpru.ru_utime.tv_sec+(tmpru.ru_utime.tv_usec/1000000.0);
  *tlib = tmpru.ru_stime.tv_sec+(tmpru.ru_stime.tv_usec/1000000.0);
}

static void initData()
{ int j;

  for(j = 0; j < N; j++) {
    v[j].a = (short int)random();
    v[j].c = 100 - v[j].a;
  }
  
}

int main()
{ double f1, fl1, f2, fl2;
  int i, j, k;
  short int t;

  for(j = 0; j < INDIRS; j++)
    indices[j] = random() % N;

#ifndef NOINIT
  initData();
#endif
  
  t = 0;

  myGetTime(&f1, &fl1);

  for(k = 0; k < NUM_REPS; k++)
    for(j = 0; j < INDIRS; j++) {
      i = indices[j];
      t += v[i].a + v[i].c;
    }

  myGetTime(&f2, &fl2);
  printf("R: %d\nTu: %lf  Tlib: %lf\n", t, f2-f1, fl2-fl1);

}
