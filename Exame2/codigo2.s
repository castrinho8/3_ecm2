.text 0x80000080

mainrut:

#Cargamos a razon da interrupcion/excepcion e saimos se non é unha interrupcion hardware(codigo 0).
  
  mfc0 $k0, $13
  andi $t0, $k0, 60
  srl $t0, $t0, 2
  bne $t0, $0, salirrut
  

#Cargamos el valor de receiver control y lo almacentamos en nuevo.
    
  li $t0, 0xffff0000
  
  lw $t2, 0($t0)
  andi $t2, $t2, 1
  beq $t2, $0, salirrut

#Cargamos el valor de receiver data y lo almacenamos en dato.
  lw $t2, 4($t0)
  
  slti $t5, $t2, 97   # Se a caracter pulsado e menor ca 97 (a en ascii)
  slti $t6, $t2, 123  # Se o caracter pulsado e menor ca 123 ({ en ascii)

  slt $t5, $t5, $t6   # Se se cumple a condicion $t5(1 se é menor ca 97) < $t6(1 se é menor ca 123)

  beq $t5, $0, salirrut #Se non esta no rango salese.

  la $t5, contador
  lw $t6, 0($t5)

  addi $t6, $t6, 1

  sw $t6, 0($t5)

salirrut:
  
#Conseguimos a direccion da instruccion que causa
#    a instruccion e devolvemos o control o programa.
  mfc0 $k0, $14
  addi $k0, $k0, 4
  rfe
  jr $k0
  
  
.text
.globl main

main:
addi $s0 , $0 , 0
addi $s1 , $0 , 0
addi $s5 , $0 , 1
addi $s2 , $0 , 1000
addi $s3 , $0 , 3
addi $s4 , $0 , 0

count1:
add $s0 , $s0 , $s5
addi $s1 , $s1 , 1
addi $s5 , $s5 , 1
bne $s1 , $s2 , count1
addi $s1 , $0 , 0
addi $s4 , $s4 , 1
bne $s4 , $s3 , count1

fin:

la $t2, contador
lw $a0, 0($t2)

addi $a0, $a0, 48 #Transformar a caracter o valor do contador sumandolle o valor ascii de 0.

jal printstring

addi $v0 , $0 , 10
syscall


printstring:

  add $t2, $0, $a0 #Almacenamos o caracter a imprimir.
  
  li $t0, 0xffff0000

xready:  
  lw $t3, 8($t0)
  andi $t3, $t3, 1
  beq $t3, $0, xready

  li $t4, 0xffff000C #Cargamos la direccion para imprimir
  sb $t2, 0($t4)     # Lo almacenamos en la direccion.
  
  jr $ra


 
.data
contador: .word 0