.text 0x80000080

mainrut:

#Guardamos los registros ($t0, $t1, $t2) que vamos a utilizar.
  addi $sp, $sp, -12
  sw $t0, 0($sp)
  sw $t1, 4($sp)
  sw $t2, 8($sp)

#Cargamos a razon da interrupcion/excepcion e saimos se é unha interrupcion hardware(codigo 0).
  
  mfc0 $k0, $13
  andi $t0, $k0, 60
  srl $t0, $t0, 2
  bne $t0, $0, salirrut
  

#Cargamos el valor de receiver control y lo almacentamos en nuevo.
    
  addi $t0, $0, 0xffff0000
  la $t1, nuevo

  lw $t2, 0($t0)
  andi $t2, $t2, 1
  sw $t2, 0($t1)

#Cargamos el valor de receiver data y lo almacenamos en dato.
  la $t1, dato

  lw $t2, 4($t0)
  sw $t2, 0($t1)

salirrut:
  
#Cargar o estado anterior dos rexistros utilizados.
  lw $t0, 0($sp)
  lw $t1, 4($sp)
  lw $t2, 8($sp)
  addi $sp, $sp, 12


#Conseguimos a direccion da instruccion que causa
#    a instruccion e devolvemos o control o programa.
  mfc0 $k0, $14
  addi $k0, $k0, 4
  rfe
  jr $k0
  
  
.text
.globl main

#--------------------
# Programa principal
#-------------------

main:
  la $t0, nuevo
  la $t7, dato
  
  addi $t5, $0, 0x3b

loop:
  
  lw $t8, 0($t0)
  bne $t8, $0, mostrar
  j loop

mostrar:
  sw $0,0($t8)
  
  sw $a1, 0($t7) #Cargamos el dato.
  beq $t5, $a1, salir #Si es ESC salimos.
  
  jal printstring #Imprimimos el dato.
  
  j loop
  
salirmain:
  addi $v0 , $0 , 10
  syscall
  
  
#----------------------
#SUBRUTINA PrintString
#----------------------

printstring:

  add $t1, $0, $a1 #Almacenamos el tamaño en $t0
  add $t2, $0, $a0 #Almacenamos la direccion de la cadena.
  
  li $t0, 0xffff0000

xready:
  beq $t1, $0, salirps
  
  lw $t3, 8($t0)
  andi $t3, $t3, 1
  beq $t3, $0, xready

  li $t4, 0xffff000C #Cargamos la direccion para imprimir
  lb $t5, 0($t2)     #Cargamos el caracter correspondiente
  sb $t5, 0($t4)     # Lo almacenamos en la direccion.
  
  addi $t2, $t2, 1
  addi  $t1, $t1, -1 
  
  j xready
  
salirps:
  jr $ra

.data 
  dato:  .word 0
  nuevo: .word 0