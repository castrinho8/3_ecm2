lui $k1, 0x1001
ori $k1, $k1, 0x0000

sw $v0, 0($k1)
sw $a0, 4($k1)

mfc0 $k0, $13
sra $k0, $k0, 2
andi $v0, $k0, 0x000F
beq $v0, $0, echo
j net

net:
lw $v0, 0($k1)
lw $a0, 4($k1)
mfc0 $k0, $14
addi $k0, $k0, 4
rfe
jr $k0

echo:
lui $a0, 0xffff
ori $a0, $a0, 0x0000

lw $v0, 0($a0)
andi $v0, $v0, 1
beq $v0, $0, exit

lw $v0, 4($a0)
sw $v0, 12($a0)
j net